package com.hernan.main;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

public class Main {

	public static void main(String[] args) {
		Automovil auto1 = new Automovil();
		auto1.setModelo("206");
		auto1.setMarca("Peugeot");
		auto1.setPrecio(new BigDecimal("200000.00").setScale(2));
		auto1.setPuertas(4);
		
		Automovil auto2 = new Automovil();
		auto2.setModelo("208");
		auto2.setMarca("Peugeot");
		auto2.setPrecio(new BigDecimal("250000.00").setScale(2));
		auto2.setPuertas(5);
		
		Moto moto1 = new Moto();
		moto1.setModelo("Titan");
		moto1.setMarca("Honda");
		moto1.setPrecio(new BigDecimal("60000.00").setScale(2));
		moto1.setCilindrada("125c");
		
		Moto moto2 = new Moto();
		moto2.setModelo("YBR");
		moto2.setMarca("Yamaha");
		moto2.setPrecio(new BigDecimal("80500.50").setScale(2));
		moto2.setCilindrada("160c");
		
		List<Vehiculo> listVehiculos = new ArrayList();
		listVehiculos.add(auto1);
		listVehiculos.add(moto1);
		listVehiculos.add(auto2);
		listVehiculos.add(moto2);
		
		
		for (Vehiculo vehiculo : listVehiculos) {
			System.out.println(vehiculo.toString());
		}
		Collections.sort(listVehiculos);
		
		System.out.println("====================================================");
		System.out.println("Veh�culo m�s caro: "+ listVehiculos.get(0).getMarca()+" "+listVehiculos.get(0).getModelo());
		System.out.println("Veh�culo m�s barato: "+listVehiculos.get(listVehiculos.size()-1).getMarca()+" "+listVehiculos.get(listVehiculos.size()-1).getModelo());
		
		for (Vehiculo vehiculo : listVehiculos) {
			if(vehiculo.getModelo().contains("Y")) {
			System.out.println("Veh�culo que contiene en el modelo la letra �Y�: "+vehiculo.getMarca()+" "+vehiculo.getModelo()+" $"+String.valueOf(vehiculo.getPrecio()));
			}
		}
		System.out.println("====================================================");
		System.out.println("=========             EXTRA            =============");
		System.out.println("====================================================");
		
		
		for (Vehiculo vehiculo : listVehiculos) {
			System.out.println(vehiculo.getMarca()+" "+vehiculo.getModelo());
		}
		

	}

}
