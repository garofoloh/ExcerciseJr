package com.hernan.main;

import java.math.BigDecimal;

public abstract class Vehiculo implements Comparable {
	private String marca;
	private String modelo;
	private BigDecimal precio;

	public String getMarca() {
		return marca;
	}

	public void setMarca(String marca) {
		this.marca = marca;
	}

	public String getModelo() {
		return modelo;
	}

	public void setModelo(String modelo) {
		this.modelo = modelo;
	}

	public BigDecimal getPrecio() {
		return precio;
	}

	public void setPrecio(BigDecimal precio) {
		this.precio = precio;
	}

	
	
	
}
