package com.hernan.main;

public class Moto extends Vehiculo {
	private String cilindrada;

	public String getCilindrada() {
		return cilindrada;
	}

	public void setCilindrada(String cilindrada) {
		this.cilindrada = cilindrada;
	}

	@Override
	public String toString() {
		return "Marca: "+super.getMarca()+ " // Modelo: "+super.getModelo()+" // cilindrada: " + getCilindrada() 
		+ " // Precio: $"+String.valueOf(super.getPrecio());
	}

	

	@Override
	public int compareTo(Object o) {
		int resultado =0;
		if(this.getPrecio().compareTo(((Vehiculo) o).getPrecio()) == -1) {
			 resultado = 1;
		}else if(this.getPrecio().compareTo(((Vehiculo) o).getPrecio()) == 1) {
			 resultado = -1;
		}else {
			 resultado = 0;
		}
		return resultado;
	}

	

}
