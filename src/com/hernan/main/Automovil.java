package com.hernan.main;

public class Automovil extends Vehiculo {
	private int puertas;

	public int getPuertas() {
		return puertas;
	}

	public void setPuertas(int puertas) {
		this.puertas = puertas;
	}

	public String toString() {
		return "Marca: "+super.getMarca()+ " // Modelo: "+super.getModelo()+" // Puertas: " + getPuertas() 
		+ " // Precio: $"+super.getPrecio().toString();
	}

	@Override
	public int compareTo(Object o) {
		int resultado =0;
		if(this.getPrecio().compareTo(((Vehiculo) o).getPrecio()) == -1) {
			 resultado = 1;
		}else if(this.getPrecio().compareTo(((Vehiculo) o).getPrecio()) == 1) {
			 resultado = -1;
		}else {
			 resultado = 0;
		}
		return resultado;
	}
}
